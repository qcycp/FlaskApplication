This is a `Flask` application sample.  

## How to run the application on develop environment  
$ virtualenv venv  
$ source venv/bin/activate  
$ pip install -r requirements.txt  
$ export FLASK_APP=main.py  
$ flask run --host 0.0.0.0 --port 5000

## How to deploy in local vm  
$ cd deploy  
$ sh deploy.sh  
$ docker-compose up -d

## How to formal release  
$ cd deploy  
$ make

#### Release files  
##### All files would be stored in deploy/$(APP_NAME), including  
1. docker-compose.yml  
2. $(APP_NAME).A0.0.0.githash.tar  
3. mysql.tar  

##### All files would be zipped in $(APP_NAME).A0.0.0.githash.tar.xz  
You can unzip the file by:  
```
tar Jxvf $(APP_NAME).A0.0.0.githash.tar.xz
```

## Test  
* execute command in docker container  
    * docker exec -it sample bash  
    * python3.6 -m pytest -vv -s test --cov=. --cov-config=test/setup.cfg --cov-report=html --html=coverage/log.html  
* execute command in host  
    * docker exec -it sample bash -c "python3.6 -m pytest -vv -s test --cov=. --cov-config=test/setup.cfg --cov-report=html --html=coverage/log.html"  
* test report will be generated to coverage/log.html  
* coverage report will be generated in coverage folder, and the summary page is coverage/index.html  
* copy reports from container to host  
    * docker cp sample:/app/coverage .  

## Sample api  
GET: curl -X GET http://localhost:5000/api/test?id=1  
POST: curl -X POST -d "data=test" http://localhost:5000/api/test  
PUT: curl -X PUT -d "data=test2" http://localhost:5000/api/test/1  
DELETE: curl -X DELETE http://localhost:5000/api/test/1  

## Note  
1. backend will listen on port 5000  
2. mysql will listen on port 3306  
3. persistent data will be stored at /opt/docker-software/$(APP_NAME)/
