FROM ubuntu:16.04

ENV DB_DATABASE "sample"
ENV DB_ROOT_PASSWORD "faca123456"

COPY app /app
WORKDIR /app

ADD wait-for /usr/bin/wait-for

# Install python3.6, pip, nginx, supervisor, and flask application
RUN apt-get update && \
    apt-get install --no-install-recommends -y software-properties-common && \
    add-apt-repository ppa:deadsnakes/ppa && \
    apt-get update && \
    apt-get install --no-install-recommends -y python3.6 curl nginx supervisor python3.6-dev libmysqlclient-dev libssl-dev build-essential netcat cron run-one && \
    curl https://bootstrap.pypa.io/get-pip.py | python3.6 && \
    pip install -r /app/requirements.txt --no-cache-dir && \
    apt-get --purge autoremove -y software-properties-common python3.6-dev build-essential curl && \
    rm -rf /var/lib/apt/lists/*

# Setup nginx
RUN rm /etc/nginx/sites-enabled/default
COPY nginx/conf.d /etc/nginx/conf.d
RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

# Setup supervisord
RUN mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

ADD run-cron.sh /etc/run-cron.sh
RUN chmod 0775 /etc/run-cron.sh
ADD cron_tasks /etc/cron.d/cron_tasks
RUN chmod 0644 /etc/cron.d/cron_tasks && \
    crontab /etc/cron.d/cron_tasks
