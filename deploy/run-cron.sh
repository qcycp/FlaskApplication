#!/bin/bash

echo 'cron starting...'
printenv | sed 's/^\(.*\)$/export \1/g' > /app/project_env.sh
chmod 0775 /app/project_env.sh
/usr/sbin/cron -f
