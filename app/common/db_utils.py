import mysql.connector
import os
import traceback
from typing import Any, Optional, Tuple
from app.foundation import logger

def get_db() -> Optional[Tuple[Any, Any]]:
    try:
        DB_HOST = os.environ.get('DB_HOST') or "db:3306"
        host = DB_HOST.split(':')[0]
        port = int(DB_HOST.split(':')[1])
        conn = mysql.connector.connect(
            host=host,
            port=port,
            user='root',
            passwd=os.environ.get('DB_ROOT_PASSWORD')
        )

        cursor = conn.cursor()
        return conn, cursor
    except:
        logger.error(traceback.format_exc())
        return None, None

def check_database() -> bool:
    try:
        db_exist = False
        conn, cursor = get_db()
        if conn is not None:
            cursor.execute("SHOW DATABASES")
            for row in cursor:
                if row[0] == os.environ.get('DB_DATABASE'):
                    db_exist = True
                    break
            conn.close()
    except:
        logger.error(traceback.format_exc())

    return db_exist

def create_database() -> None:
    try:
        conn, cursor = get_db()
        if conn is not None:
            db = os.environ.get('DB_DATABASE')
            sql = f"CREATE DATABASE {db} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci"
            cursor.execute(sql)
            conn.commit()
        conn.close()
    except:
        logger.error(traceback.format_exc())
