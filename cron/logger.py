import os
import sys
#append path in order to import other modules
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import logging
from logging.handlers import RotatingFileHandler
from config import LOGDIR

def get_logger(name: str = 'cron_tasks') -> logging.Logger:
    if not os.path.exists(LOGDIR):
        os.makedirs(LOGDIR, exist_ok=True)
    logger = logging.Logger(name)
    formatter = logging.Formatter('%(asctime)s - [%(filename)s:%(lineno)s] - %(levelname)s %(message)s ')

    fileHandler = RotatingFileHandler(os.path.join(LOGDIR, name+'.log'), mode='a', maxBytes=50*1024*1024, backupCount=10, encoding='utf-8', delay=0)
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)
    return logger
