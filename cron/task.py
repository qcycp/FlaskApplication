import os
import sys
#task.py is executed isolated by another process
#append the path in order to import other modules
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from app.common.logger import Logger

if __name__ == '__main__':
    logger = Logger("cron_task").get_logger()
    logger.info('cron task executing...')
